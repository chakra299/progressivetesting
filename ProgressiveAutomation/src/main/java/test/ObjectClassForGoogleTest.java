package test;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.GoogleSearchPage;

public class ObjectClassForGoogleTest {

	public static WebDriver webDriver;
	GoogleSearchPage googleSearchPage;
	
	@BeforeTest
	public void invokeBrowser() {

		System.setProperty("webdriver.chrome.driver", "./libs/chromedriver");
		webDriver = new ChromeDriver();	
		webDriver.manage().window().maximize();
		webDriver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		String string = webDriver.getTitle();
		System.out.println(string);		
	}
	public void callBrowser() {
		webDriver.get("https://www.google.com/");
		webDriver.navigate().refresh();
		webDriver.manage().window().maximize();
		
	}
		

	@Test(priority = 0)
	public void googlePageTesting() throws InterruptedException {
		callBrowser();
		
		GoogleSearchPage googleSearchPage = new GoogleSearchPage(webDriver);
		googleSearchPage.enterTextBox("Automation Step by Step");
		googleSearchPage.clickOnGoogleSearch();
	}

	@AfterTest
	public void tearDownTest() {
		//webDriver.close();
		//webDriver.quit();
		//System.out.println("Testing have done");

	}

}
