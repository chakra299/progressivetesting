package test;

import java.util.concurrent.TimeUnit;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import pages.AddDriverPage;
import pages.AutoZipeCodePage;
import pages.DriverInformationPage;
import pages.HomePages;
import pages.PreviousInsuranceRecordPage;
import pages.SnapshotPage;
import pages.StartMyQuotePage;
import pages.VehicleInformationPage;



public class ProgressiveWebPagesTest {

	public static WebDriver webDriver;
	HomePages homePage;
	AutoZipeCodePage autoZCodePage;
	StartMyQuotePage startMyQuotePage;
	VehicleInformationPage vehicleInformationPage;
	DriverInformationPage driverInformationPage;
	AddDriverPage addDriverPage;
	PreviousInsuranceRecordPage previousInsuranceRecordPage;
	SnapshotPage snapshotPage;
	
	@BeforeTest
	public void initialize()  {
		System.setProperty("webdriver.chrome.driver", "./libs/chromedriver");
		
		webDriver = new ChromeDriver();
		
		webDriver.get("https://google.com");
		
		webDriver.manage().window().maximize();
		webDriver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		String string = webDriver.getTitle();
		System.out.println(string);
		
		
	}
	public void callBrowser() {
		webDriver.get("https://www.progressive.com/");
		webDriver.navigate().refresh();
		webDriver.manage().window().maximize();
		
	}

	@Test(priority = 0)
	public void firstPageTest(){
	    callBrowser();
		HomePages homePage = new HomePages(webDriver);
		homePage.clickOnAutoLink();
	}
	
	@Test(priority = 1)
	public void secondPageTest() {
		AutoZipeCodePage autoZCodePage = new AutoZipeCodePage(webDriver);
		autoZCodePage.enterZIPCode("85302");
		
	}
	@Test(priority = 2)
	public void thirdPageTest() throws InterruptedException{
		
		StartMyQuotePage startMyQuotePage = new StartMyQuotePage(webDriver);
		Thread.sleep(1000);
		startMyQuotePage.enterFirstName("Prince");
		Thread.sleep(1000);
		startMyQuotePage.enterMiddleInitial("B");
		Thread.sleep(1000);
		startMyQuotePage.enterLastName("Saud");
		Thread.sleep(1000);
		startMyQuotePage.selectSuffix("Jr");
		Thread.sleep(1000);
		startMyQuotePage.enterDOB("12311980");
		Thread.sleep(1000);
		startMyQuotePage.enterStreetNumberAndName("5555 E vally viewln");
		Thread.sleep(1000);
		startMyQuotePage.enterAptNumber("222");
		Thread.sleep(1000);
		startMyQuotePage.clickOnPostBOX();
		Thread.sleep(1000);
		startMyQuotePage.clickStartMyQuote();
		Thread.sleep(10000);	
		
	}
	@Test(priority = 3)
	public void forthPageTest() throws InterruptedException{

			VehicleInformationPage vehicleInformationPage = new VehicleInformationPage (webDriver);
			
			vehicleInformationPage.selectVehicleYear("2017"); 
			Thread.sleep(1000);
			vehicleInformationPage.selectVehicleMake("Toyota"); 
			Thread.sleep(1000);

			vehicleInformationPage.selectVehiclModel("Rav4");

			Thread.sleep(1000);

			vehicleInformationPage.selectVehiclePrimaryUse("Personal (to/from work or school, errands, pleasure");
			Thread.sleep(1000);
			vehicleInformationPage.selectVehicleOwnOrLease("Own");
			Thread.sleep(1000);
			vehicleInformationPage.selectVehicleOwnTimePeriod("3 years - 5 years");
			Thread.sleep(1000);
			//vehicleInformationPage.selectVehicleBlindSpotWarning("Y"); 
			vehicleInformationPage.selectVehicleBlindSpotWarning(); 
			vehicleInformationPage.clickOnDone();
			Thread.sleep(1000);
			vehicleInformationPage.clickContinueTOnextPage();
		} 
		
	
	@Test(priority = 4)
	public void fifthPageTest() throws InterruptedException{
		DriverInformationPage driverInformationPage = new DriverInformationPage(webDriver);
		driverInformationPage.selectOnGender();
		Thread.sleep(500);
		driverInformationPage.selectMaritalStatus("Single");
		Thread.sleep(500);
		driverInformationPage.selectLevelOfEducation("College Degree");
		Thread.sleep(500);
		driverInformationPage.selectEmploymentStatus("Employed");
		Thread.sleep(500);
		driverInformationPage.selectOccupation("Software Developer");
		Thread.sleep(500);
		driverInformationPage.enterSocialSecurityNumber("801-46-3585");
		Thread.sleep(500);
		driverInformationPage.selectPrimaryResidence("Own home");
		Thread.sleep(500);
		driverInformationPage.selectMovedAddress("no");
		Thread.sleep(500);
		driverInformationPage.selectLicenseStatus("Valid");
		Thread.sleep(500);
		driverInformationPage.selectYearLicensed("3 years or more");
		Thread.sleep(500);
		driverInformationPage.clickonAccidentOrClaim();
		Thread.sleep(500);
		driverInformationPage.clickOnTicketOrViolation();
		Thread.sleep(500);
		driverInformationPage.continueForNextPage();
		Thread.sleep(500);
		driverInformationPage.continueToReviewPage();
		Thread.sleep(500);
		driverInformationPage.continueToReviewNextPage();
	}
	
		@Test(priority = 5)
		public void sixthPageTest() throws InterruptedException{
			AddDriverPage addDriverPage = new AddDriverPage(webDriver);
			addDriverPage.clickOnContinueDriverPage();				
	}
		
		@Test(priority = 6)
		public void seventhPageTest() throws InterruptedException{
			PreviousInsuranceRecordPage previousInsuranceRecordPage = new PreviousInsuranceRecordPage(webDriver);
			Thread.sleep(500);
			previousInsuranceRecordPage.autoInsuranceTodayWtihProgressive();
			Thread.sleep(500);
			previousInsuranceRecordPage.timePeriodWithCurrentCompany("1 to 3 years");
			Thread.sleep(500);
			previousInsuranceRecordPage.bodilyInjuryLimit("$25,000/$50,000");
			Thread.sleep(500);
			previousInsuranceRecordPage.nonAutoPolicy();
			Thread.sleep(500);
			previousInsuranceRecordPage.autoPolicy();
			Thread.sleep(500);
			previousInsuranceRecordPage.enterEmailAddress("prince@gmail.com");
			Thread.sleep(500);
			previousInsuranceRecordPage.continueForNextPage();
			Thread.sleep(500);
		}
		@Test(priority = 7)
		public void eightPageTest() throws InterruptedException{
			SnapshotPage snapshotPage = new SnapshotPage (webDriver);
			Thread.sleep(500);
			snapshotPage.clickEnrollInSnapshot();
			Thread.sleep(500);
			snapshotPage.continueForNextPage();
			
		}
	@AfterTest
	public void TeardownTest() {
		//webDriver.close();
		//webDriver.quit();
	}

}
