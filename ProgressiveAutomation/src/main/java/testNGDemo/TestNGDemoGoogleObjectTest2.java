package testNGDemo;



import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;


public class TestNGDemoGoogleObjectTest2 {

	 WebDriver webDriver = null;

	// 1:invoke the browser
	@BeforeTest
	public void invokeBrowser()  {

		// set the system path to point to the chromedriver file
		System.setProperty("webdriver.chrome.driver", "libs/chromedriver");
		// Utilizing the driver global variable and creating a new chrome driver
		webDriver = new ChromeDriver();
		// navigate to cerotid website

	}

//2: Perform Action
	@Test
	public  void performActions2() {
		webDriver.get("https://www.google.com");
		// Maximize the window

		//webDriver.findElement(By.xpath("//input[@name='q']")).sendKeys("Automation Step by step");
		//webDriver.findElement(By.xpath("//input[@name='btnK']")).sendKeys(Keys.RETURN);
		webDriver.findElement(By.name("q")).sendKeys("Automation Step by step");
		webDriver.findElement(By.name("btnK")).sendKeys(Keys.RETURN);
		
	}

//3: End Test Case
	@AfterTest
	public void tearDownTest() {
		
		webDriver.close();
		webDriver.quit();
		System.out.println("Testing have done");
		
	}

	
}
