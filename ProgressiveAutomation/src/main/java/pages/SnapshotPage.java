package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class SnapshotPage {

	WebDriver webDriver;

	public SnapshotPage(WebDriver webDriver) {
		this.webDriver = webDriver;
		PageFactory.initElements(webDriver, this);
	}

	@FindBy(how = How.ID, using = "SnapshotEnrollment40Edit_embedded_questions_list_SnapshotPolicyEnrollment_N")
	WebElement enrollInSnapshot;
	
	@FindBy(how = How.XPATH, using = "//button[contains(text(), 'Continue')]") public WebElement clickOnContinue;


	public void clickEnrollInSnapshot() {
		enrollInSnapshot.click();
	}
	public void continueForNextPage() {
		clickOnContinue.click();
	}
}


