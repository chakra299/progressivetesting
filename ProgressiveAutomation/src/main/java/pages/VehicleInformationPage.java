package pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class VehicleInformationPage{
	WebDriver webDriver;

	public VehicleInformationPage(WebDriver webDriver)  {
		this.webDriver = webDriver;
		PageFactory.initElements(webDriver, this);
	
	}
	public void selectVehicleYear(String selectYear) throws InterruptedException{
		WebElement yearElement = webDriver.findElement(By.xpath("//*[@name='VehiclesNew_embedded_questions_list_Year']"));
		List<WebElement> yearList = yearElement.findElements(By.tagName("li"));
		for (WebElement selectBuildYear : yearList) {
			if(selectBuildYear.getText().equalsIgnoreCase(selectYear)) {
				selectBuildYear.click();
				Thread.sleep(5000);
				break;
			}
			
		}
		
	}
	
	public void selectVehicleMake(String selectMake)throws InterruptedException {
		WebElement makeElement = webDriver.findElement(By.xpath("//*[@name='VehiclesNew_embedded_questions_list_Make']"));
		List<WebElement> makeList = makeElement.findElements(By.tagName("li"));
		for (WebElement selectMakeList : makeList) {
			if(selectMakeList.getText().equalsIgnoreCase(selectMake)) {
				selectMakeList.click();
				Thread.sleep(5000);
				break;
			}
			
		}
		
	}
	
	public void selectVehiclModel(String selectModel)throws InterruptedException {
		WebElement modelElement = webDriver.findElement(By.xpath("//*[@name='VehiclesNew_embedded_questions_list_Model']"));
		List<WebElement> modelList =  modelElement.findElements(By.tagName("li"));
		for (WebElement selectModelList :  modelList) {
			if( selectModelList.getText().equalsIgnoreCase(selectModel)) {
				 selectModelList.click();
				 Thread.sleep(5000);
				 break;
			}
			
		}
	}

	

	@FindBy(how = How.XPATH, using = "//option[contains (text(), 'Personal (to/from work or school, errands, pleasure')]")
	public WebElement selectVehiclePrimaryUse;

	@FindBy(how = How.XPATH, using = "//option[contains (text(), 'Own')]")
	public WebElement selectVehicleOwnOrLease;
	
	@FindBy(how = How.XPATH, using = "//option[contains (text(), '3 years - 5 years')][1]")
	public WebElement selectVehicleOwnTimePeriod;
	
	@FindBy(how = How.XPATH, using = "//input[@value='Y']") 
	public WebElement selectVehicleBlindSpot;	
	@FindBy(how = How.XPATH, using = "//button[contains(text(), 'Done')]") public WebElement clickOnDone;

	@FindBy(how = How.XPATH, using = "//button[contains(text(), 'Continue')]") public WebElement continueForNextPage;

	public void selectVehiclePrimaryUse(String primaryUse) {
		Select selectUse = new Select(selectVehiclePrimaryUse);
		selectUse.selectByVisibleText(primaryUse);
	}


	public void selectVehicleOwnOrLease(String selectOwnOrLease) {
		Select select = new Select(selectVehiclePrimaryUse);
		select.selectByVisibleText(selectOwnOrLease);
	}

	public void selectVehicleOwnTimePeriod(String selectOwnTime) {
		Select select = new Select(selectVehicleOwnTimePeriod);
		select.selectByVisibleText(selectOwnTime);

	}
	
	

	public void selectVehicleBlindSpotWarning() {
		selectVehicleBlindSpot.click();
	}
	public void clickOnDone() {
		clickOnDone.click();
	}

	public void clickContinueTOnextPage() {
		continueForNextPage.click();

	}
	
}
