package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class GoogleSearchPage {

	 WebDriver webDriver;

	public GoogleSearchPage(WebDriver webDriver) {
		this.webDriver = webDriver;
		PageFactory.initElements(webDriver, this);
	}

	@FindBy(how = How.XPATH, using = "//input[@name='q']")
	public WebElement textBox;

	@FindBy(how = How.TAG_NAME, using = "//input[@data-ved='0ahUKEwiWiIn3jqfpAhWSup4KHbXyCHcQ4dUDCA0']")
	public WebElement googleSearchButton;

	public void enterTextBox(String title) {
		textBox.sendKeys(title);;
	}

	public void clickOnGoogleSearch() {
		googleSearchButton.click();
	}
}
