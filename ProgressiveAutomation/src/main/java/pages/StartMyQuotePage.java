package pages;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class StartMyQuotePage {

	WebDriver webDriver;

	public StartMyQuotePage(WebDriver webDriver) {
		this.webDriver = webDriver;
		PageFactory.initElements(webDriver, this);
	}

	@FindBy(how = How.ID, using = "NameAndAddressEdit_embedded_questions_list_FirstName")
	public WebElement enterFirstName;
	@FindBy(how = How.ID, using = "NameAndAddressEdit_embedded_questions_list_MiddleInitial")
	public WebElement enterMiInitial;
	@FindBy(how = How.ID, using = "NameAndAddressEdit_embedded_questions_list_LastName")
	public WebElement enterLastName;
	@FindBy(how = How.ID, using = "NameAndAddressEdit_embedded_questions_list_Suffix")
	public WebElement selectSuffix;
	@FindBy(how = How.ID, using = "NameAndAddressEdit_embedded_questions_list_DateOfBirth")
	public WebElement enterDOB;
	@FindBy(how = How.ID, using = "NameAndAddressEdit_embedded_questions_list_MailingAddress")
	public WebElement enterStreetAddress;
	@FindBy(how = How.ID, using = "NameAndAddressEdit_embedded_questions_list_ApartmentUnit")
	public WebElement enterAptNumber;
	@FindBy(how = How.ID, using = "NameAndAddressEdit_embedded_questions_list_MailingZipType")
	public WebElement clickOnPostBOX;
	@FindBy(how = How.XPATH, using = "//button[contains(text(),'Okay, start my quote.')]")
	public WebElement clickStartMyQuote;

	public void enterFirstName(String firstName) {
		enterFirstName.sendKeys(firstName);
	}

	public void enterMiddleInitial(String middleName) {
		enterMiInitial.sendKeys(middleName);
	}

	public void enterLastName(String lastName) {
		enterLastName.sendKeys(lastName);
	}

	public void selectSuffix(String suffix) {
		Select suffixSelect = new Select(selectSuffix);
		suffixSelect.selectByVisibleText(suffix);
	}

	public void enterDOB(String dateOfBirth) {
		enterDOB.sendKeys(dateOfBirth);
		enterDOB.sendKeys(Keys.TAB);
	}
	
	
	public void enterStreetNumberAndName(String streetAddress) {
		enterStreetAddress.sendKeys(streetAddress);
		
	}
	public void enterAptNumber(String aptNumber) {
		enterAptNumber.sendKeys(aptNumber);
	}
	
	public void clickOnPostBOX() {
		clickOnPostBOX.click();
	}
	

	public void clickStartMyQuote() {
		clickStartMyQuote.click();
		
	}

}
