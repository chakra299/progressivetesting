package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class HomePages {
	WebDriver webDriver;

	public HomePages(WebDriver webDriver) {
		this.webDriver = webDriver;
		PageFactory.initElements(webDriver, this);
	}

	@FindBy(how = How.XPATH, using = "(//p[contains(text(),'Auto ')])[1]")
	WebElement autoLink;
	//@FindBy(how=How.XPATH, using="//text()[.='Log Out']/ancestor::span[1]")
	//WebElement logoutLink;

	public void clickOnAutoLink() {
		autoLink.click();
	}
	// This method to click on Logout link
	//public void clickOnLogoutLink(){
	//logoutLink.click();}

}
