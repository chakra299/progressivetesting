package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class AddDriverPage {
	
WebDriver webDriver;
	
	public AddDriverPage(WebDriver webDriver) {
		this.webDriver = webDriver;
		PageFactory.initElements(webDriver, this);
	}
		@FindBy(how = How.XPATH, using = "//button[contains(text(), 'Continue')]")
		public WebElement continueFromAddDriverPage;
		
		public void clickOnContinueDriverPage() {
			continueFromAddDriverPage.click();
		}
	}

