package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class PreviousInsuranceRecordPage {

	WebDriver webDriver;

	public PreviousInsuranceRecordPage(WebDriver webDriver) {
		this.webDriver = webDriver;
		PageFactory.initElements(webDriver, this);
	}

	@FindBy(how = How.ID, using = "FinalDetailsEdit_embedded_questions_list_InsuranceToday_Y")
	public WebElement clickOnAutoInsuranceToday;
	@FindBy(how = How.ID, using = "FinalDetailsEdit_embedded_questions_list_RecentAutoInsuranceCompanyPeriod_Label")
	public WebElement selectTimePeriod;
	@FindBy(how = How.ID, using = "FinalDetailsEdit_embedded_questions_list_BodilyInjuryLimits_Label")
	public WebElement selectBodilyInjuryLimit;
	@FindBy(how = How.ID, using = "FinalDetailsEdit_embedded_questions_list_OtherPolicies_N")
	public WebElement clickOnNonAutoPolicy;
	@FindBy(how = How.ID, using = "FinalDetailsEdit_embedded_questions_list_PriorProgressive_N")
	public WebElement clickOnAutoPolicy;
	@FindBy(how = How.ID, using = "FinalDetailsEdit_embedded_questions_list_AdvancedShopperPolicyEffectiveDate")
	public WebElement inputPolicyEffectiveDate;
	@FindBy(how = How.ID, using = "FinalDetailsEdit_embedded_questions_list_PrimaryEmailAddress")
	public WebElement inputEmailAddress;
	@FindBy(how = How.XPATH, using = "//button[contains(text(), 'Continue')]")
	public WebElement clickOnContinue;

	public void autoInsuranceTodayWtihProgressive() {
		clickOnAutoInsuranceToday.click();
	}

	public void timePeriodWithCurrentCompany(String timePeriod) {
		Select selectTime = new Select(selectTimePeriod);
		selectTime.selectByVisibleText(timePeriod);
	}

	public void bodilyInjuryLimit(String InjuryLimit) {
		Select selectInjuryLimit = new Select(selectBodilyInjuryLimit);
		selectInjuryLimit.selectByVisibleText(InjuryLimit);
	}

	public void nonAutoPolicy() {
		clickOnNonAutoPolicy.click();
	}

	public void autoPolicy() {
		clickOnAutoPolicy.click();
	}

	public void enterEmailAddress(String emailAddress) {
		inputEmailAddress.sendKeys(emailAddress);
	}

	public void continueForNextPage() {
		clickOnContinue.click();
	}

}
