package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class DriverInformationPage {

	WebDriver webDriver;

	public DriverInformationPage(WebDriver webDriver) {
		this.webDriver = webDriver;
		PageFactory.initElements(webDriver, this);
	}

	@FindBy(how = How.ID, using = "DriversAddPniDetails_embedded_questions_list_Gender_M")
	public WebElement selectGender;

	@FindBy(how = How.ID, using = "DriversAddPniDetails_embedded_questions_list_MaritalStatus")
	public WebElement selectMaritalStatus;

	@FindBy(how = How.ID, using = "DriversAddPniDetails_embedded_questions_list_HighestLevelOfEducation")
	public WebElement selectLevelOfEducation;

	@FindBy(how = How.ID, using = "DriversAddPniDetails_embedded_questions_list_EmploymentStatus")
	public WebElement selectEmploymentStatus;

	@FindBy(how = How.ID, using = "DriversAddPniDetails_embedded_questions_list_Occupation")
	public WebElement selectOccupation;

	@FindBy(how = How.ID, using = "DriversAddPniDetails_embedded_questions_list_SocialSecurityNumber")
	public WebElement inputSSN;

	@FindBy(how = How.ID, using = "DriversAddPniDetails_embedded_questions_list_PrimaryResidence")
	public WebElement selectPrimaryResidence;

	@FindBy(how = How.ID, using = "DriversAddPniDetails_embedded_questions_list_HasPriorAddress")
	public WebElement selectMovedAddress;

	@FindBy(how = How.ID, using = "DriversAddPniDetails_embedded_questions_list_LicenseStatus")
	public WebElement selectLicenseStatus;

	@FindBy(how = How.ID, using = "DriversAddPniDetails_embedded_questions_list_DriverYearsLicensed")
	public WebElement selectYearLicensed;

	@FindBy(how = How.ID, using = "DriversAddPniDetails_embedded_questions_list_HasAccidentsOrClaims_N")
	public WebElement clickonAccidentOrClaim;

	@FindBy(how = How.ID, using = "DriversAddPniDetails_embedded_questions_list_HasTicketsOrViolations_N")
	public WebElement clickOnTicketOrViolation;

	@FindBy(how = How.XPATH, using = "//button[contains(text(), 'Continue')]")
	public WebElement continueForNextPage;
	
	@FindBy(how = How.XPATH, using = "//button[contains(text(), 'Continue')]")
	public WebElement continueToReviewPage;
	
	@FindBy(how = How.XPATH, using = "//button[contains(text(), 'Continue')]")
	public WebElement continueToReviewNextPage;


	public void selectOnGender() {
		selectGender.click();
	}

	public void selectMaritalStatus(String maritalStatus) {
		Select selectStatus = new Select(selectMaritalStatus);
		selectStatus.selectByVisibleText(maritalStatus);
	}

	public void selectLevelOfEducation(String leveOfEducation) {
		Select selectEducation = new Select(selectLevelOfEducation);
		selectEducation.selectByVisibleText(leveOfEducation);
	}

	public void selectEmploymentStatus(String employment) {
		Select selectEmployment = new Select(selectEmploymentStatus);
		selectEmployment.selectByVisibleText(employment);
	}

	public void selectOccupation(String occupation) {
		Select select = new Select(selectOccupation);
		select.selectByVisibleText(occupation);
	}

	public void enterSocialSecurityNumber(String ssn) {
		inputSSN.sendKeys(ssn);
	}

	public void selectPrimaryResidence(String primaryResidence) {
		Select selectResidence = new Select(selectPrimaryResidence);
		selectResidence.selectByVisibleText(primaryResidence);
	}

	public void selectMovedAddress(String movingAddress) {
		Select selectMovingAdd = new Select(selectMovedAddress);
		selectMovingAdd.selectByVisibleText(movingAddress);
	}

	public void selectLicenseStatus(String licenseStatus) {
		Select selectLicense = new Select(selectLicenseStatus);
		selectLicense.selectByVisibleText(licenseStatus);
	}

	public void selectYearLicensed(String yearLicened) {
		Select selectYear = new Select(selectYearLicensed);
		selectYear.selectByVisibleText(yearLicened);
	}

	public void clickonAccidentOrClaim() {
		clickonAccidentOrClaim.click();
	}

	public void clickOnTicketOrViolation() {
		clickOnTicketOrViolation.click();
	}

	public void continueForNextPage() {
		continueForNextPage.click();
	}
	
	public void continueToReviewPage() {
		continueToReviewPage.click();
	}
	public void continueToReviewNextPage() {
		continueToReviewNextPage.click();
	}

}