package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class AutoZipeCodePage {
	WebDriver webDriver;
	

	public AutoZipeCodePage(WebDriver webDriver) {
		this.webDriver = webDriver;
		PageFactory.initElements(webDriver, this);
        
	}

	@FindBy(how = How.ID, using = "zipCode_overlay")
	public WebElement enterZipCode;
	@FindBy(how = How.ID, using = "qsButton_overlay")
	public WebElement cickGetAQuote;

	public void enterZIPCode(String zipCode) {
		enterZipCode.sendKeys(zipCode);
		cickGetAQuote.click();
		
	}

	/*
	public void clickOnGetQuoate() {
		cickGetAQuote.click();
	}
*/
}
