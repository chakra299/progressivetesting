package frameWorkMethod;


import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Find {
	
	      public static void main(String[] args) {
	    	  System.setProperty("webdriver.chrome.driver", "libs/chromedriver");
	          WebDriver driver = new ChromeDriver();
	          try {
	              driver.get("https://www.google.com");

	              // Get element with tag name 'div'
	              WebElement element = driver.findElement(By.tagName("div"));

	              // Get all the elements available with tag name 'p'
	              List<WebElement> elements = element.findElements(By.tagName("p"));
	              for (WebElement e : elements) {
	                  System.out.println(e.getText());
	              }
	          } finally {
	              driver.quit();
	          }
	      }

}
